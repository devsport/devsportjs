function rehydrate(ctor, obj) {
	if(!obj || (obj.constructor.prototype === ctor.prototype))
		return obj;
	var copy = Object.create(ctor.prototype);
	Object.assign(copy, obj);
	if(ctor.prototype.rehydrated)
		ctor.prototype.rehydrated.call(obj, copy);
	return copy;
};
module.exports = rehydrate;
