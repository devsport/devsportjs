const readline = require("readline");

var comms = { };
module.exports = comms;

readline.createInterface({input: process.stdin})
.on("line", l => {
	try {
		var d = JSON.parse(l);
		if(d.from) {
			comms.inbox = comms.inbox || { };
			comms.inbox[d.from] = d;
		}
	} catch(err) {
		console.log(err);
	}
});

comms.send = function(msg) {
	process.stdout.write(JSON.stringify(msg) + "\n");
}
