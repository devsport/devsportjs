module.exports = {
	w: 16,
	h: 10,
	delin: 8,
	speed: 500,
	save: false,
	endturn: null,
	restart: 7200
}

try { Object.assign(module.exports, require("./config_local")); }
catch(err) { }
