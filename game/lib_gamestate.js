const grid2d = require("./lib_grid2d");
const mazegen = require("./lib_mazegen");
const myutil = require("./lib_myutil");
const v2d = require("./lib_v2d");

function gamestate(config) {
	this.config = config;

	this.turn = this.turn || 0;
	this.players = this.players || [];
	this.fx = this.fx || [];

	mazegen.call(this, new v2d(config.w, config.h));
	this.delinearize(config.delin);
}
gamestate.prototype = Object.create(mazegen.prototype);
gamestate.prototype.constructor = gamestate;
module.exports = gamestate;

gamestate.prototype.findspawn = function() {
	var seen = {};
	var q = [];
	for(var y = 0; y < this.size.y; y++)
		for(var x = 0; x < this.size.x; x++) {
			var v = new v2d(x, y);
			var t = this.get(v);
			if(!t)
				continue;
			seen[v] = true;
			if(t.t) {
				q.push(v);
				for(var d = 0; d < 1; d += 0.25) {
					var dv = v2d.azimuth(d).nn();
					for(var a = v.add(dv); !this.get(a); a = a.add(dv).mod(this.size))
						q.push(a);
				}
			}
		}
	if(!q.length)
		while(true) {
			var v = new v2d(myutil.rand(this.size.x), myutil.rand(this.size.y));
			if(!this.get(v)) {
				q.push(v);
				break;
			}
		}

	var prob = 1;
	var total = 0;
	var safe;
	while(q.length) {
		myutil.shuffle(q);
		var next = [];
		for(var i = 0; i < q.length; i++) {
			v = q[i];
			if(seen[v])
				continue;
			seen[v] = true;
			for(var d = 0; d < 1; d += 0.25)
				next.push(v2d.azimuth(d).add(v).nn().mod(this.size));
			if(safe) {
				v.p = prob;
				total += prob;
				safe.push(v);
			}
		}
		if(!safe)
			safe = [];
		else
			prob *= 1.5;
		q = next;
	}
	if(!safe || !safe.length)
		return;

	var q = Math.random() * total;
	while(q > 0)
		q -= safe.shift().p;
	if(safe.length)
		return safe[0];
};

gamestate.prototype.addfx = function(obj) {
	this.fx.push(obj);
}

gamestate.prototype.spawntank = function(idx, id, cb) {
	var loc = idx[id];
	if(loc)
		return cb(loc);

	var loc = this.findspawn();
	if(!loc)
		return;

	var tank = {
		t: true,
		o: id,
		r: myutil.rand(4) / 4,
		tw: {v: true}
	};
	this.set(loc, tank);
	this.addfx({x: loc.x, y: loc.y, o: id, t: "s"});
	loc.tank = tank;
	idx[id] = loc;
	return cb(loc);
};

gamestate.prototype.tick = function(inbox) {
	this.fx = []

	var playidx = {};
	for(var i = 0; i < this.players.length; i++)
		playidx[this.players[i].n] = i;

	var tankidx = {};
	for(var x = 0; x < this.size.x; x++)
		for(var y = 0; y < this.size.y; y++) {
			var v = new v2d(x, y);
			var i = this.get(v);
			if(i)
				delete(i.tw);
			if(i && i.t) {
				i.v = (i.v || 0) + 1;
				if(i.v >= 20)
					this.players[i.o].s += 0.025;
				v.tank = i;
				tankidx[i.o] = v;
			}
		}

	var tanks = [];
	for(var name in inbox)
		if(inbox.hasOwnProperty(name)) {
			var msg = inbox[name];
			var pid = playidx[name];
			var t;
			if(typeof(pid) != "undefined")
				this.spawntank(tankidx, pid, t => {
					var cmd = msg.data;
					if((cmd.t <= this.turn) && (cmd.x == t.x) && (cmd.y == t.y))
						t.cmd = cmd.c;
					if(msg.id > this.players[pid].c) {
						this.players[pid].c = msg.id;
						t.adv = true;
					}
					tanks.push(t);
				});
			else {
				pid = this.players.length;
				this.spawntank(tankidx, pid, t => {
					this.players.push({i: pid, n: name, s: 0, c: msg.id});
					playidx[name] = pid;
				});
			}
		}
	myutil.shuffle(tanks);

	var modone = n => n + 1000 - Math.floor(n + 1000);

	for(var i = 0; i < tanks.length; i++) {
		var t = tanks[i];
		if(t.cmd != "s")
			continue;
		var dist = 0;
		var dir = v2d.azimuth(t.tank.r).nn();
		var hit;
		for(var a = t.add(dir).mod(this.size);
			!(hit = this.get(a)); a = a.add(dir).mod(this.size)) {
			dist++;
			this.addfx({x: a.x - dir.x, y: a.y - dir.y, r: t.tank.r, t: "t"});
		}
		if(dist < 1) {
			t.tank.die = true;
			this.players[t.tank.o].s--;
			this.addfx({x: t.x, y: t.y, r: t.tank.r, t: "b"});
			continue;
		}
		this.addfx({x: t.x, y: t.y, r: t.tank.r, t: "f"});
		if(hit.t && hit.t) {
			if((hit.r == modone(t.tank.r + 0.5)) && (hit.cmd != "shoot")) {
				this.addfx({x: t.add(dir).x, y: t.add(dir).y, r: hit.r, t: "h"});
				this.players[t.tank.o].s--;
				t.tank.die = true;
				continue;
			}
			this.players[t.tank.o].s += (t.tank.o != hit.o) ? 1 : -1;
			hit.die = true;
		}
		this.addfx({x: a.x - dir.x, y: a.y - dir.y, r: t.tank.r, t: "h"});
	}

	for(var i = 0; i < tanks.length; i++) {
		var t = tanks[i];
		if(t.tank.die)
			continue;
		if(t.cmd == "f") {
			var to = t.add(v2d.azimuth(t.tank.r)).nn();
			if(!this.get(to)) {
				var tw = v2d.azimuth(t.tank.r).neg();
				t.tank.tw = {x: tw.x, y: tw.y};
				this.set(to, t.tank);
				this.set(t);
				t.x = to.x;
				t.y = to.y;
			}
		} else if(t.cmd == "l") {
			t.tank.r = modone(t.tank.r - 0.25);
			t.tank.tw = {r: 0.25};
		} else if(t.cmd == "r") {
			t.tank.r = modone(t.tank.r + 0.25);
			t.tank.tw = {r: -0.25};
		}
		if(t.adv)
			this.addfx({
				x: t.x + (t.tank.tw && t.tank.tw.x || 0) / 2,
				y: t.y + (t.tank.tw && t.tank.tw.y || 0) / 2,
				t: "a"
			});
	}

	for(var k in tankidx)
		if(tankidx.hasOwnProperty(k)) {
			var t = tankidx[k];
			if(t.tank.die) {
				this.set(t);
				this.addfx({x: t.x, y: t.y, o: t.tank.o, t: "d"});
			}
		}
};
