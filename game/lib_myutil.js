const crypto = require("crypto");

var myutil = {};
module.exports = myutil;

myutil.dump = function() {
	return process.stderr.write(require("util").inspect(arguments) + "\n");
};

myutil.rand = function(max) {
	if(max < 2)
		return 0;
	var bits = 0;
	for(var mask = 1; mask < max; mask *= 2)
		bits++;
	while(true) {
		var num = parseInt(crypto.randomBytes(Math.ceil(bits / 8))
			.toString("hex"), 16) % mask;
		if(num < max)
			return num;
	}
};

myutil.shuffle = function(arr) {
	for(var i = 0; i < arr.length; i++) {
		var j = myutil.rand(arr.length);
		var t = arr[i];
		arr[i] = arr[j];
		arr[j] = t;
	}
	return arr;
};
