const fs = require("fs");
const os = require("os");
const path = require("path");

var db = { };
module.exports = db;

const GAMEDB = ".gamedb";
function dbfile(num) {
	var str = "" + num;
	return path.join(GAMEDB, ("0000000000" + str).substr(-10) + ".json");
}
db.dbfile = dbfile;

function findlatest(cb, min, max, mode) {
	if(mode) {
		if(min == max)
			return cb(undefined, min);
		var mid = Math.ceil((min + max) / 2);
		return fs.access(dbfile(mid), fs.constants.R_OK, err => {
			if(err && err.code == "ENOENT")
				return findlatest(cb, min, mid - 1, 1);
			if(err)
				return cb(err);
			return findlatest(cb, mid, max, 1);
		});
	}
 	min = min || -1;
	max = max || 1;
	fs.access(dbfile(max), fs.constants.R_OK, err => {
		if(err && err.code == "ENOENT")
			return findlatest(cb, min, max, 1);
		if(err)
			return cb(err);
		return findlatest(cb, max, max * 2);
	});
}
db.findlatest = findlatest;

function tryload(id, cb) {
	if(id < 0)
		return cb();
	return fs.readFile(dbfile(id), (err, data) => {
		if(err)
			return tryload(id - 1, cb);
		try { return cb(JSON.parse(data)); }
		catch(err) { return tryload(id - 1, cb); }
	});
}
function loadlatest(cb) {
	return findlatest((err, id) => {
		if(err)
			return cb(err);
		return tryload(id, cb);
	});
}
db.loadlatest = loadlatest;

function save(id, data, cb) {
	return fs.mkdir(GAMEDB, () =>
		fs.writeFile(dbfile(id),
			JSON.stringify(data), cb));
}
db.save = save;
