const dirs = require("./lib_dirs");
const grid2d = require("./lib_grid2d");
const myutil = require("./lib_myutil");
const v2d = require("./lib_v2d");

function planmaze(size) {
	var maze = new grid2d(size);
	maze.fillfrom(function() { return { }; });

	var st = [ new v2d(myutil.rand(maze.size.x), myutil.rand(maze.size.y)) ];
	while(st.length) {
		var v = st.pop();
		var cell = maze.get(v);

		if(cell.v)
			continue;
		cell.v = true;

		if(v.d)
			cell[dirs[v.d].o.n] = true;

		var allow = [ ];
		(v.x > 0) && allow.push("w");
		(v.x < maze.size.x - 1) && allow.push("e");
		(v.y > 0) && allow.push("n");
		(v.y < maze.size.y - 1) && allow.push("s");
		myutil.shuffle(allow);

		for(var j = 0, l = allow.length; j < l; j++) {
			var d = allow[j];
			var vn = v.add(dirs[d]);
			vn.d = d;
			st.push(vn);
		}
	}

	maze.scan(function(v) { delete maze.get(v).v; });
	return maze;
};

function mazegen(size) {
	grid2d.call(this, size);
	this.fill(true);
	var maze = planmaze(this.size.mul(0.5).floor());
	var self = this;
	maze.scan(function(v) {
		var n = v.mul(2).add(dirs.se);
		self.set(n);
		var mcell = maze.get(v);
		for(var dn in mcell)
			self.set(n.add(dirs[dn]));
	});
}
mazegen.prototype = Object.create(grid2d.prototype);
mazegen.prototype.constructor = mazegen;
module.exports = mazegen;

function delinearizecore(grid, len, visited, p) {
	var a = 0;
	var q = [ p ];
	for(var i = 0; i < q.length; i++) {
		var v = q[i];

		if(visited.get(v))
			continue;
		visited.set(v, true);
		a++;

		var notcorner
			= (!grid.get(v.add(dirs.n)) == !grid.get(v.add(dirs.s)))
			&& (!grid.get(v.add(dirs.e)) == !grid.get(v.add(dirs.w)))
			&& (!grid.get(v.add(dirs.e)) != !grid.get(v.add(dirs.s)));
		if((a > len) && notcorner) {
			grid.set(v);
			continue;
		}

		var r = [];
		for(var d in dirs.d4) {
			var vn = v.add(dirs[d]);
			if(grid.get(vn))
				r.push(vn);
		}
		myutil.shuffle(r);
		for(var j = 0; j < r.length; j++)
			q.push(r[j]);
	}
};
mazegen.prototype.delinearize = function(len) {
	var visited = new grid2d(this.size);
	var starts = [ ];
	this.scan(function(v) { starts.push(v); });
	myutil.shuffle(starts);
	for(var i = 0, l = starts.length; i < l; i++)
		delinearizecore(this, len, visited, starts[i]);
};
