========================================================================
                             T A N K M A Z E
                                Game Host
------------------------------------------------------------------------

This is the host component for the TankMaze game.  Run it through the
"host" agent to connect it to a hub and host a TankMaze game.

------------------------------------------------------------------------
-- CONFIGURATION

The config.js script returns a configuration object that can be used
to control the parameters of the game.  Settings need to be defined
before the game is started.

Available configuration keys:

- "w":
- "h":
	The width and height, respectively, of the game playing
	field.  These should both be even numbers, or the wraparound
	navigation may not work.

- "delin":
	The size of walls after delinearization.  Higher values will
	create longer corridors with fewer loops and diversions, while
	lower values will create a more porous terrain.

- "speed":
	Duration of each turn in milliseconds; larger values create
	slower game pace.

- "save":
	Boolean that determines whether the game is persisted to disk.
	If false, the game remains in-memory only and will be reset on
	a crash or exit.  If true, the game is persisted to disk, and
	an existing game is loaded if found, so the game can be resumed
	after a crash or restart.  The files on disk can be kept as a
	recording of the game action.

- "endturn":
	The turn after which the game will freeze.  This is to be used
	for tournament play, when the game ends after a predetermined
	amount of time, and final scores are displayed.

- "restart":
	The number of turns after which the game will restart
	automatically.  This is to be used for practice/demo servers,
	to simulate multiple games in series, and generate fresh
	terrain.

========================================================================
