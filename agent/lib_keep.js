function keepalive(conn, timeout, log) {
	function activity() {
		if(conn.keepalive_pinger)
			clearTimeout(conn.keepalive_pinger);
		if(conn.keepalive_killer)
			clearTimeout(conn.keepalive_killer);
		if(conn.readyState > 1)
			return;
		conn.keepalive_pinger = setTimeout(() => {
			if(conn.readyState == 1)
				conn.ping();
		}, (Math.random() * 0.5 + 0.25) * timeout);
		conn.keepalive_killer = setTimeout(() => {
			if(conn.readyState < 3)
				log("inactivity timeout");
			conn.terminate()
		}, timeout);
	}
	"ping pong message".split(" ").forEach(x => conn.on(x, activity));
	activity();
}
module.exports = keepalive;
