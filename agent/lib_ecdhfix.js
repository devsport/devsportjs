const tls = require("tls");

try {
	tls.connect({port:0, ecdhCurve:"auto"})
		.on("error", () => {});
	tls.DEFAULT_ECDH_CURVE = "auto";
} catch(err) { }
