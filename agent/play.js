require("./lib_engine")({
	opt: function(opts, kdf) {
		opts.passwd = opts.passwd || process.env.DEVSPORT_PASSWD
		"url user".split(" ").forEach(x => {
			if(!opts[x])
				throw("--" + x + " option required");
		});
		if(!opts.passwd && !opts.passhash)
			throw("--passwd option required");
		opts.passhash = opts.passhash || kdf(opts.passwd, opts.user);
	},
	ws: function(opts, wsopts) {
		wsopts.headers.Authorization = "Basic " + new Buffer(
			opts.user + ":" + opts.passhash).toString("base64");
	},
	out: function(opts, data) {
		return {me: opts.user, data: data};
	}
});
