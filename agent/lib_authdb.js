const crypto = require("crypto");
const fs = require("fs");
const os = require("os");
const path = require("path");

var db = { };
module.exports = db;

function sha(str, enc) {
	var sha = crypto.createHash("sha256");
	sha.update(str);
	return sha.digest(enc);
}
db.sha = sha;

function checkname(str) {
	str = "" + str;
	if((str.length > 16) || str.match(/[^A-Za-z0-9]/))
		throw("invalid name " + JSON.stringify(str));
	return str;
}
db.checkname = checkname;

function dbpath(parts) {
	var my = []
	for(var i = 0; i < parts.length; i++)
		my.push(checkname(parts[i]));
	my.unshift(".devsportjs-hub");
	my.unshift(os.homedir());
	return path.join.apply(path, my) + ".json";
}
db.dbpath = dbpath;

function load(parts, cb) {
	var p;
	try { p = dbpath(parts); }
	catch(err) { return cb(err); }
	return fs.readFile(p, (err, data) => {
		if(err)
			return cb(err);
		try { data = JSON.parse(data); }
		catch(err) { return cb(err); }
		return cb(undefined, data);
	});
}
db.load = load;

function mktree(p, cb) {
	var d = p.split(path.sep);
	if((d.length <= 1) || !d.some(x => x != ""))
		return cb();
	if(d[0] == "")
		d[0] = path.sep;
	d.pop();
	d = path.join.apply(path, d);
	return mktree(d, () => fs.mkdir(d, 0o700, cb));
}
function save(parts, data, cb) {
	var p;
	try { p = dbpath(parts); }
	catch(err) { return cb(err); }
	mktree(p, () =>
		fs.writeFile(p, JSON.stringify(data), (err) => {
			if(err)
				return cb(err);
			data.new = true;
			return cb(undefined, data);
		}));
}
db.save = save;

function hmac(salt, pass) {
	var hmac = crypto.createHmac("sha256", pass);
	hmac.update(salt);
	return hmac.digest("base64");
}
db.hmac = hmac;

function setpass(obj, pass) {
	obj.pass_salt = crypto.randomBytes(16).toString("base64");
	obj.pass_hash = hmac(obj.pass_salt, pass);
}
db.setpass = setpass;

function tse(a, b) { return crypto.timingSafeEqual(new Buffer(a), new Buffer(b)); }
function checkpass(obj, pass, onsave, cb) {
	if(obj.pass_salt && obj.pass_hash)
		return cb(tse(hmac(obj.pass_salt, pass), obj.pass_hash)
			? undefined : "auth failed");
	if(obj.pass_plain) {
		if(!tse(pass, obj.pass_plain))
			return false;
		delete(obj.pass_plain);
		setpass(obj, pass);
		return onsave(err => cb(err));
	}
	return cb("auth failed");
}
db.checkpass = checkpass;

function tryauth(parts, pass, cb) {
	return load(parts, (err, data) => {
		if(err) {
			data = { };
			setpass(data, pass);
			return save(parts, data, cb);
		}
		checkpass(data, pass,
			scb => save(parts, data, scb),
			err => cb(err, err ? undefined : data));
	});
}
db.tryauth = tryauth;
