const myepoch = 1514764800000;
function getnow() { return new Date().getTime() - myepoch; }

var nextid = getnow();

function logctx() {
	var now = getnow();
	if(now > nextid)
		nextid = now;
	this.id = nextid++;
}
module.exports = logctx;

logctx.prototype.log = function(msg) {
	console.log("[" + this.id + "] " + msg);
};
