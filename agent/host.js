require("./lib_engine")({
	opt: function(opts, kdf) {
		opts.auth = opts.auth || process.env.DEVSPORT_HOSTAUTH
		if(!opts.url)
			throw("--url=... option required");
		if(!opts.auth && !opts.authhash)
			throw("--auth=... option required");
		if(!opts.authhash)
			opts.authhash = kdf(opts.auth, opts.url.replace(/.*\//, ""));
	},
	ws: function(opts, wsopts) {
		wsopts.headers.Authorization = "Bearer " + opts.authhash;
	}
});
