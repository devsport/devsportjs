const child_process = require("child_process");
const crypto = require("crypto");
const hpkp = require("./lib_hpkp");
const minimist = require("minimist");
const msgpack = require("msgpack-lite");
const readline = require("readline");

require("./lib_ecdhfix");

module.exports = function(mods) {
	var opts = minimist(process.argv.slice(2));
	opts.retry = opts.retry || 1000;
	if(mods.opt) {
		function kdf(key, salt) {
			process.stderr.write("calculating auth key... ");
			var start = new Date().getTime();
			var buff = crypto.pbkdf2Sync(key, salt, 65536,
				256, "sha256");
			var end = new Date().getTime();
			process.stderr.write((end - start) + "ms\n");
			return buff.toString("base64");
		}
		mods.opt(opts, kdf);
	}

	if(opts.chdir)
		process.chdir(opts.chdir);

	function subopts(pref) {
		var found = {};
		for(var k in opts)
			if(k.startsWith(pref + "_") && opts.hasOwnProperty(k))
				found[k.substr(pref.length + 1)] = opts[k];
		return found;
	}

	var engine;
	var wsconn;

	function wssend(msg) {
		if(!wsconn || (wsconn.readyState != 1))
			return;
		try {
			var data = msgpack.encode(JSON.parse(msg));
			wsconn.send(data);
		} catch(err) {
			console.log("error writing to websock: " + err);
		}
	}

	if(opts.stdio) {
		engine = process.stdout;
		readline.createInterface({input: process.stdin})
			.on("line", wssend);
	}
	else if(opts.cmd) {
		var cmdopts = subopts("cmd");
		cmdopts.shell = true;
		cmdopts.stdio = ["pipe", "pipe", "pipe"];
		if(mods.cmd)
			mods.cmd(opt, cmdopts);

		var cmdproc;
		function endcmd(msg) {
			if(msg) console.log(msg);
			try { wsconn.close(); }
			catch(err) { }
			if(cmdproc)
				try { cmdproc.kill(); }
				catch(err) { }
			cmdproc = undefined;
		}
		setInterval(() => {
			if(cmdproc)
				return;
			cmdproc = child_process.spawn(opts.cmd, cmdopts);
			cmdproc.on("close", (code, signal) =>
				endcmd("engine exited " + JSON.stringify({code: code, signal: signal})));
			cmdproc.on("error", err => endcmd("engine error: " + err));
			engine = cmdproc.stdin;
			engine.on("error", err => endcmd("engine error: " + err));
			readline.createInterface({input: cmdproc.stdout}).on("line", wssend);
			readline.createInterface({input: cmdproc.stderr}).on("line",
				l => console.log("worker: " + l));
		}, opts.retry);
	}
	else
		throw("--cmd or --stdio is required");
	
	var wsopts = subopts("ws");
	wsopts.protocol = "binary";
	wsopts.headers = wsopts.headers || {};
	if(mods.ws)
		mods.ws(opts, wsopts);

	setInterval(() => {
		if(wsconn)
			return;
		wsconn = hpkp(opts, opts.url, wsopts);
		wsconn.on("open", () => {
			console.log("websock open");
		});
		wsconn.on("error", err => {
			console.log("websock error: " + err);
			if(wsconn)
				wsconn.terminate();
		});
		wsconn.on("close", () => {
			console.log("websock closed");
			if(wsconn)
				wsconn.terminate();
			wsconn = undefined;
		});
		wsconn.on("message", msg => {
			if(!engine)
				return;
			try {
				var data = msgpack.decode(msg);
				if(mods.out)
					data = mods.out(opts, data);
				engine.write(JSON.stringify(data) + "\n");
			} catch(err) {
				console.log("error writing to engine: " + err);
			}
			
		});
	}, opts.retry);
}
