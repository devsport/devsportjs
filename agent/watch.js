require("./lib_engine")({
	opt: function(opts, kdf) {
		if(!opts.url)
			throw("--url option required");
	},
	ws: function(opts, wsopts) {
	},
	out: function(opts, data) {
		return {data: data};
	}
});
