const crypto = require("crypto");
const ws = require("ws");
const x509 = require("asn1.js-rfc5280");
const x509_cert = x509.Certificate;
const x509_spki = x509.SubjectPublicKeyInfo;

function getpinshas(incert, out) {
	out = out || { };
	if(!incert || incert.pinsha_seen)
		return out;
	incert.pinsha_seen = true;

	var cert = x509_cert.decode(incert.raw, "der");
	var spki = cert.tbsCertificate.subjectPublicKeyInfo;
	var der = x509_spki.encode(spki, "der");
	var sha = crypto.createHash("sha256");
	sha.update(der);
	out[sha.digest("base64")] = true;

	out = getpinshas(incert.issuerCertificate, out);
	delete(incert.pinsha_seen);
	return out;
}

function hpkp(conf, url, opts) {
	opts.rejectUnauthorized = !conf.nopki;
	var conn = new ws(url, opts);
	if(!conf.pins)
		return conn;
	conn.on("upgrade", req => {
		var chain = req.socket.getPeerCertificate(true);
		var shas = Object.keys(getpinshas(chain));
		shas.sort();
		for(var i = 0; i < shas.length; i++)
			if(conf.pins.hasOwnProperty(shas[i])
				|| conf.pins.hasOwnProperty(shas[i].replace("=", ""))) {
				console.log("HPKP matched: " + shas[i]);
				return;
			}
		console.log("no HPKP match: " + shas.join(", "));
		req.socket.destroy();
	});
	return conn;
}
module.exports = hpkp;
