const authdb = require("./lib_authdb");
const keepalive = require("./lib_keep");
const logctx = require("./lib_logctx");
const minimist = require("minimist");
const msgpack = require("msgpack-lite");
const ws = require("ws");

var opts = minimist(process.argv.slice(2));
opts.host = opts.host || "127.0.0.1";
opts.port = opts.port || 8080;
opts.timeout = opts.timeout || 60000;

var hubstats = { inmsg: 0, outmsg: 0, inbytes: 0, outbytes: 0 };
setInterval(() => {
	if(hubstats.inmsg || hubstats.outmsg
		|| hubstats.inbytes || hubstats.outbytes) {
		console.log("[stats] in: " + (hubstats.inmsg || 0) + " msg = "
			+ (hubstats.inbytes || 0) + " bytes; out: "
			+ (hubstats.outmsg || 0) + " msg = "
			+ (hubstats.outbytes || 0) + " bytes");
		hubstats.inmsg = 0;
		hubstats.outmsg = 0;
		hubstats.inbytes = 0;
		hubstats.outbytes = 0;
	}
}, 60000);

function recvobj(ctx, cb) {
	return ctx.conn.on("message", raw => {
		hubstats.inmsg++;
		hubstats.inbytes += raw.length;
		try { cb(msgpack.decode(raw)); }
		catch(err) {
			ctx.log("error: " + err);
			ctx.conn.terminate();
		}
	});
}
function sendobj(ctx, msg) {
	if(ctx.conn.readyState == 1) {
		var mp = msg.mp || msgpack.encode(msg.raw);
		msg.mp = mp;
		hubstats.outmsg++;
		hubstats.outbytes += mp.length;
		ctx.conn.send(mp);
	}
	else
		ctx.conn.terminate();
}
			
var games = { };

function hub_host(ctx) {
	var old = games[ctx.gameid];
	if(old && old.rm)
		old.rm();

	ctx.clients = { };
	games[ctx.gameid] = ctx;

	function allclients(cb) {
		for(var k in ctx.clients)
			if(ctx.clients.hasOwnProperty(k))
				cb(ctx.clients[k], k);
	}

	ctx.rm = function() {
		if(games[ctx.gameid] == ctx)
			delete(games[ctx.gameid]);
		allclients(x => x.conn.terminate());
		ctx.conn.terminate();
	};
	ctx.conn.on("close", () => {
		ctx.log("closed");
		ctx.rm();
	});

	recvobj(ctx, msg => {
		allclients(x => sendobj(x, {raw: msg}));
		ctx.state = msg;
	});

	keepalive(ctx.conn, opts.timeout, x => ctx.log(x));
}

function hub_join(ctx) {
	var game = games[ctx.gameid];

	var uid = ctx.userid || ("_" + ctx.id);
	var old = game.clients[uid];
	if(old)
		old.conn.terminate();
	game.clients[uid] = ctx;

	if(game.state)
		sendobj(ctx, {raw: game.state});

	ctx.conn.on("close", () => {
		ctx.log("closed");
		if(game.clients[uid] == ctx)
			delete(game.clients[uid]);
	});

	if(ctx.userid)
		recvobj(ctx, msg => sendobj(game,
			{raw: {id: ctx.id, from: uid, data: msg}}));

	keepalive(ctx.conn, opts.timeout, x => ctx.log(x));
}

opts.handleProtocols = p => {
	if(p.some(x => x == "binary"))
		return "binary";
	return false;
};

opts.verifyClient = (info, cb) => {
	var ctx = new logctx();
	ctx.log("new connection");

	var logcb = (ok, code, msg) => {
		ctx.log((code ? (code + " ") : "") + msg);
		return ok ? cb(ok) : cb(ok, code, msg);
	};

	var req = info.req;
	req.hub_ctx = ctx;
	var parts = req.url.split("/").filter(x => x.match(/\S/));
	ctx.log("url: " + req.url);
	if(parts.length != 2)
		return cb(false, 400, "invalid uri");

	if(parts[0] == "host") {
		if(!parts[1] || parts[1].match(/[^A-Za-z0-9]/))
			return logcb(false, 403, "game name forbidden");
		ctx.gameid = parts[1];

		var auth = (req.headers.authorization || "").split(" ", 2);
		if(auth[0] != "Bearer")
			return logcb(false, 401, "bearer auth required");
		return authdb.tryauth(["game", parts[1]], auth[1], (err, data) => {
			if(err)
				return logcb(false, 401, err);
			ctx.authdata = data;
			ctx.hub_cb = hub_host;
			return logcb(true, 0, "host " + JSON.stringify(parts[1]));
		});
	}
	if(parts[0] == "join") {
		function getgame(cb) {
			if(!parts[1] || parts[1].match(/[^A-Za-z0-9]/))
				return logcb(false, 403, "game name forbidden");
			if(!games[parts[1]])
				return logcb(false, 404, "game not available");
			ctx.gameid = parts[1];
			return cb();
		}

		if(!req.headers.authorization)
			return getgame(() => {
				ctx.hub_cb = hub_join;
				return logcb(true, 0, "watch " + JSON.stringify(parts[1]));
			});

		var auth = (req.headers.authorization || "").split(" ", 2);
		if(auth[0] != "Basic")
			return logcb(false, 401, "auth must be basic for players");
		try { auth = new Buffer(auth[1], "base64").toString("ascii").split(":", 2); }
		catch(err) { return logcb(false, 401, err); }
		ctx.userid = auth[0];
		ctx.log("userid: " + auth[0]);

		return authdb.tryauth(["player", auth[0]], auth[1], (err, data) => {
			if(err)
				return logcb(false, 401, err);
			return getgame(() => {
				ctx.authdata = data;
				ctx.hub_cb = hub_join;
				return logcb(true, 0, "join " + JSON.stringify(parts[1])
					+ " as " + JSON.stringify(auth[0]));
			});
		});
	}

	return logcb(false, 400, "invalid uri");
};

var hub = new ws.Server(opts);
hub.on("listening", () => console.log("[master] listening on " + opts.port));
hub.on("error", err => { throw(err); });

hub.on("connection", (conn, req) => {
	var ctx = req.hub_ctx;
	if(!ctx)
		return conn.terminate();
	conn.on("error", err => {
		ctx.log("error: " + err);
		conn.terminate();
	});
	ctx.conn = conn;
	ctx.req = req;
	if(ctx.hub_cb)
		return ctx.hub_cb(ctx);
	return conn.terminate();
});
