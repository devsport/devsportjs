////////////////////////////////////////////////////////////////////////
// UTILITY

var BLK = 64;

function setvis(el, show) { el.style.display = show ? "" : "none"; }
function togvis(el) { setvis(el, el.style.display == "none"); }

var escapeHtml = (function() {
	var ents = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		"\"": "&quot;",
		"'": "&#39;",
		"/": "&#x2F;",
		"`": "&#x60;",
		"=": "&#x3D;"
	};
	return function(str) {
		return String(str).replace(/[&<>"'`=\/]/g,
			function(s) { return ents[s]; });
	};
})();

function frozen(state) {
	return state && state.config && state.config.endturn
		&& state.turn && (state.turn >= state.config.endturn);
}
////////////////////////////////////////////////////////////////////////
// IMAGE HANDLING

var pending = { };
function isready() {
	for(var k in pending)
		if(pending.hasOwnProperty(k))
			return;
	return true;
}

var images = { };
(function() {
	var names = "wall floor tank".split(" ");
	for(var i = 0; i < names.length; i++)
		(function(n) {
			pending["image:" + n] = true;
			var img = new Image();
			img.onload = function() {
				delete(pending["image:" + n]);
			};
			img.src = n + ".png";
			images[n] = img;
		})(names[i]);
})();

var goldangle = Math.PI * (3 - Math.sqrt(5));
function getcolor(id) {
	var t = goldangle * id;
	function v(d) { return Math.floor(128 + 127 * Math.cos(
		t + d * 2 / 3 * Math.PI)); }
	return {r: v(0), g: v(1), b: v(2)};
}

var gettank = (function() {
	var tanx = {};
	function gentank(rx, gx, bx) {
		rx /= 255;
		gx /= 255;
		bx /= 255;
		var rlut = [];
		var glut = [];
		var blut = [];
		for(var i = 0; i < 128; i++) {
			rlut.push(Math.floor(rx * i * 2));
			glut.push(Math.floor(gx * i * 2));
			blut.push(Math.floor(bx * i * 2));
		}
		for(var i = 0; i < 128; i++) {
			rlut.push(Math.floor(255 * (i / 128) + rx * (128 - i) * 2));
			glut.push(Math.floor(255 * (i / 128) + gx * (128 - i) * 2));
			blut.push(Math.floor(255 * (i / 128) + bx * (128 - i) * 2));
		}

		var tmp = document.createElement("canvas");
		tmp.width = images.tank.width;
		tmp.height = images.tank.height;
		var ctx = tmp.getContext("2d");
		ctx.drawImage(images.tank, 0, 0);
		var data = ctx.getImageData(0, 0, tmp.width, tmp.height);
		for(var i = 0; i < tmp.width * tmp.height; i++) {
			data.data[i * 4 + 2] = blut[data.data[i * 4]];
			data.data[i * 4 + 1] = glut[data.data[i * 4]];
			data.data[i * 4 + 0] = rlut[data.data[i * 4]];
		}
		ctx.putImageData(data, 0, 0);
		return tmp;
	}
	return function(id) {
		return tanx[id] = tanx[id] || gentank(
			getcolor(id).r,
			getcolor(id).g,
			getcolor(id).b);
	}
})();

////////////////////////////////////////////////////////////////////////
// LEADER BOARD

var frametimes = [];
var leaderboard = document.getElementById("leaders");
(function() {
	var all = document.getElementsByTagName("*");
	for(var i = 0; i < all.length; i++)
		all[i].addEventListener("click", function() {
			togvis(leaderboard);
		}, false);
})();

function updateboard(state) {
	var parts = []

	parts.push("<div id=\"fps\">" + Math.round(frametimes.length / 5) + " FPS</div>");

	if(!state) {
		leaderboard.innerHTML = parts.join("");
		return setvis(leaderboard);
	}

	var players = [];
	for(var i = 0; i < state.players.length; i++)
		players.push(state.players[i]);
	players.sort(function(a, b) { return b.s - a.s; });
	var rows = [{r: 255, g: 255, b: 255, id: "#", name: "Name", score: "Score"}];
	for(var i = 0; i < players.length; i++) {
		var p = players[i];
		var c = getcolor(p.i);
		rows.push({r: c.r, g: c.g, b: c.b, id: p.i, name: escapeHtml(p.n), score: p.s.toFixed(3)});
	}
	for(var i = 0; i < rows.length; i++) {
		var r = rows[i];
		rows[i] = "<tr style=\"color: rgb(" + [r.r, r.g, r.b].join(",") + ")\">"
			+ "<td class=\"r\">" + r.id + "</td>"
			+ "<td class=\"brk\">" + r.name + "</td>"
			+ "<td class=\"r\">" + r.score + "</td>"
			+ "</tr>";
	}
	parts.push("<table>" + rows.join("") + "</table>");

	parts.push("<p>Turn: " + state.turn);
	var ends = state.config && state.config.endturn;
	var reset = state.config && state.config.restart;
	if(ends && (!reset || (reset >= ends)))
		parts.push("<div class=\"small\">Ends after " + ends + "</div>");
	else if(reset)
		parts.push("<div class=\"small\">Resets after " + reset + "</div>");
	parts.push("</p>");

	leaderboard.innerHTML = parts.join("");
	if(frozen(state))
		setvis(leaderboard, 1);
}

////////////////////////////////////////////////////////////////////////
// PARTICLE EFFECT SETUP

var fxdone = {};
var particles = [];

function particlefan(fx, vel, smoke) {
	for(var z = 0; z < 20; z++) {
		var p = {
			startcolor: smoke ? {r: 128, g: 128, b: 128, a: 1} : {r: 255, g: 255, a: 1},
			endcolor: smoke ? {r: 32, g: 32, b: 32} : {r: 255},
			startx: !fx.dx ? (fx.x + 0.5) : (fx.dx > 0) ? (fx.x + 1) : fx.x,
			starty: !fx.dy ? (fx.y + 0.5) : (fx.dy > 0) ? (fx.y + 1) : fx.y,
			starts: 0.05,
			ends: 0.4
		};
		p.endx = p.startx + fx.dx * vel * (Math.random() + 0.25) + Math.random() * 2 - 1;
		p.endy = p.starty + fx.dy * vel * (Math.random() + 0.25) + Math.random() * 2 - 1;
		particles.push(p);
	}
}
var fxtypes = {};
fxtypes.f = function(fx) { return particlefan(fx, 3); };
fxtypes.b = function(fx) { return particlefan(fx, -0.5); };
fxtypes.h = function(fx) { return particlefan(fx, -0.5, true); };
fxtypes.t = function(fx) {
	var sx = !fx.dx ? (fx.x + 0.5) : (fx.dx > 0) ? (fx.x + 1) : fx.x;
	var sy = !fx.dy ? (fx.y + 0.5) : (fx.dy > 0) ? (fx.y + 1) : fx.y;
	for(var i = 0; i < 5; i++) {
		sx += fx.dx / 10;
		sy += fx.dy / 10;
		particles.push({
			startcolor: {r: 128, g: 128, b: 128, a: 1},
			endcolor: {r: 32, g: 32, b: 32},
			startx: sx,
			starty: sy,
			endx: sx + Math.random() / 2 - 0.25,
			endy: sy + Math.random() / 2 - 0.25,
			starts: 0.05,
			ends: 0.4
		});
		sx += fx.dx / 10;
		sy += fx.dy / 10;
	}
};
function tanxplode(fx, rev, c, d, q) {
	c = c || getcolor(fx.o);
	c.a = c.a || 1;
	d = d || 15;
	for(var z = 0; z < (q || 20); z++) {
		var p = {
			startcolor: c,
			endcolor: c,
			startx: fx.x + 0.25 + Math.random() * 0.5,
			starty: fx.y + 0.25 + Math.random() * 0.5,
			endx: fx.x + Math.random() * d - (d - 1) / 2,
			endy: fx.y + Math.random() * d - (d - 1) / 2,
			starts: 0.5,
			ends: 0
		};
		if(rev)
			p = {
				startcolor: c,
				endcolor: c,
				startx: p.endx,
				starty: p.endy,
				endx: p.startx,
				endy: p.starty,
				starts: p.ends,
				ends: p.starts
			};
		particles.push(p);
	}
}
fxtypes.d = tanxplode;
fxtypes.s = function(fx) { return tanxplode(fx, true); };
fxtypes.a = function(fx) {
	var c = {r: 255, g: 255, b: 255, a: 0.5};
	tanxplode(fx, false, c, 3, 5);
	tanxplode(fx, true, c, 3, 5);
};

function updateparticles(state) {
	particles = [];
	if(!state.fx || !state.fx.length)
		return;
	for(var i = 0; i < state.fx.length; i++) {
		var f = state.fx[i];
		var t = fxtypes[f.t];
		if(t) {
			f.r = f.r || 0;
			f.dx = Math.round(Math.sin(Math.PI * 2 * f.r));
			f.dy = Math.round(-Math.cos(Math.PI * 2 * f.r));
			t(f);
		}
	}
}

////////////////////////////////////////////////////////////////////////
// STATE INTERPOLATION

var states = [];
var lasttime = new Date().getTime();
var stateinterp = 0;
function statetick() {
	if(states.length < 1)
		return;

	var speed = (states.length - 5) / 10 + 1;
	if(speed < 0.75)
		speed = 0.75;
	if(speed > 2)
		speed = 2;

	var now = new Date().getTime();
	var elapsed = now - lasttime;
	lasttime = now;

	stateinterp -= elapsed / states[0].config.speed * speed;
	while(stateinterp < 0) {
		if(states.length > 1) {
			states.shift();
			if(states.length) {
				updateboard(states[0]);
				updateparticles(states[0]);
			}
			stateinterp += 1;
		}
		else
			stateinterp = 0;
	}
}


////////////////////////////////////////////////////////////////////////
// WEBSOCK POLLING

(function() {
	pending.states = true;
	var websock, wsretry, wsurl;
	function getwsurl() {
		var u = window.location.hash.substr(1);
		if(!u.match(/^wss?:\/\//)) {
			var p = window.location.pathname.split("/");
			for(var i = p.length - 1; i >= 0; i--)
				if(!p[i])
					p.splice(i, 1);
			var x = u.split("/");
			if(x[0] == "")
				p = [];
			for(var i = x.length - 1; i >= 0; i--)
				if(!x[i] || (x[i] == "."))
					x.splice(i, 1);
			for(var i = 0; i < x.length; i++)
				if(x[i] == "..")
					p.pop();
				else
					p.push(x[i]);
			u = window.location.origin.replace(/^http/, "ws")
				+ "/" + p.join("/");
		}
		return u;
	}
	function mksock() {
		function retry() {
			pending.states = true;
			updateboard();
			if(websock)
				try { websock.close(); }
				catch(err) { console.log(err); }
			if(wsretry)
				clearTimeout(wsretry);
			wsretry = setTimeout(mksock, 1000);
		}
		wsurl = getwsurl();
		websock = new WebSocket(wsurl, "binary");
		websock.binaryType = "arraybuffer";
		websock.onmessage = function(evt) {
			data = new Uint8Array(evt.data);
			var len = data.length;
			data = msgpack.decode(data);
			states.push(data);
			if(states.length == 1)
				updateboard(states[0]);
			if((states.length >= 5) || frozen(data))
				delete(pending.states);
			console.log("received " + len + " bytes for turn "
				+ data.turn + ", queued " + states.length);
		};
		websock.onclose = retry;
		websock.onerror = retry;
	}

	var cfgerr = document.getElementById("cfgerr");
	var spinner = document.getElementById("spinner");
	if(!window.msgpack)
		cfgerr.innerHTML = "msgpack-lite dependency is not deployed correctly or failed to load";
	else if(!window.location.hash)
		cfgerr.innerHTML = "set url hash to websocket address and reload";
	else {
		setvis(cfgerr);
		setvis(spinner, 1);
		mksock();
	}

	function checkwsurl() {
		setTimeout(checkwsurl, 250);
		if(websock && (getwsurl() != wsurl))
			websock.close();
	}
	checkwsurl();
})();

////////////////////////////////////////////////////////////////////////
// RENDERING

var canvascale = (function() {
	var setw, seth;
	return function(el, w, h) {
		if((el.width != w) || (el.height != h)) {
			el.width = w;
			el.height = h;
		}
		var ca = w / h;
		var wa = window.innerWidth / window.innerHeight;
		if(wa > ca) {
			h = Math.floor(window.innerHeight);
			w = Math.floor(window.innerHeight * ca);
		} else {
			w = Math.floor(window.innerWidth);
			h = Math.floor(window.innerWidth / ca);
		}
		if((setw != w) || (seth != h)) {
			el.style.width = setw = w;
			el.style.height = seth = h;
		}
	};
})();

function drawentraw(ctx, img, x, y, r) {
	ctx.save();
	try {
		ctx.translate(x * BLK, y * BLK);
		ctx.translate(BLK / 2, BLK / 2);
		ctx.rotate(Math.PI * 2 * r);
		ctx.translate(-BLK / 2, -BLK / 2);
		ctx.drawImage(img, 0, 0, BLK, BLK);
	} finally {
		ctx.restore();
	}
}

var sqrt2 = Math.sqrt(2);
function wrapdraw(x, y, tw, func) {
	if(tw && tw.x)
		x += stateinterp * tw.x;
	if(tw && tw.y)
		y += stateinterp * tw.y;
	var state = states[0];
	var sx = state.size.x;
	var sy = state.size.y;
	var rx = x > (sx - 2 - sqrt2 / 2);
	var lx = x < 2 + sqrt2 / 2;
	var by = y > (sy - 2 - sqrt2 / 2);
	var ty = y < 2 + sqrt2 / 2;
	func(x, y);
	if(lx)
		func(x + sx, y);
	if(lx && ty)
		func(x + sx, y + sy);
	if(lx && by)
		func(x + sx, y - sy);
	if(rx)
		func(x - sx, y);
	if(rx && ty)
		func(x - sx, y + sy);
	if(rx && by)
		func(x - sx, y - sy);
	if(ty)
		func(x, y + sy);
	if(by)
		func(x, y - sy);
}

function drawpart(ctx, p) {
	var sw = stateinterp;
	var ew = 1 - sw;
	var r = Math.floor((p.startcolor.r || 0) * sw + (p.endcolor.r || 0) * ew);
	var g = Math.floor((p.startcolor.g || 0) * sw + (p.endcolor.g || 0) * ew);
	var b = Math.floor((p.startcolor.b || 0) * sw + (p.endcolor.b || 0) * ew);
	var a = (p.startcolor.a || 0) * sw + (p.endcolor.a || 0) * ew;
	var c = "rgba(" + r + "," + g + "," + b + "," + a + ")";
	var x = p.startx * sw + p.endx * ew;
	var y = p.starty * sw + p.endy * ew;
	var s = (p.starts * sw + p.ends * ew) * BLK / 2;
	wrapdraw(x, y, {}, function(x, y) {
		ctx.save();
		try {
			ctx.translate(x * BLK, y * BLK);
			ctx.fillStyle = c;
			ctx.beginPath();
			ctx.arc(0, 0, s, 0, Math.PI * 2, true);
			ctx.fill();
		} finally {
			ctx.restore();
		}
	});
}

function drawent(ctx, img, x, y, r, tw) {
	if(tw && tw.r)
		r += stateinterp * tw.r;
	return wrapdraw(x, y, tw, function(x, y) {
		return drawentraw(ctx, img, x, y, r);
	});
}

function shadetext(ctx, x, y, w, t, tw) {
	return wrapdraw(x, y, tw, function(x, y) {
		ctx.font = "bold " + (BLK / 4) + "px sans-serif";
		ctx.fillStyle = "rgba(0,0,0,0.25)";
		x *= BLK;
		y *= BLK;
		w *= BLK;
		for(var dx = -1; dx <= 1; dx++)
			for(var dy = -1; dy <= 1; dy++)
				if((dx != 0) || (dy != 0))
					ctx.fillText(t, x + dx, y + dy, w)
		ctx.fillStyle = "rgba(255,255,255,0.75)";
		return ctx.fillText(t, x, y, w)
	});
}

var buildterrain = (function() {
	var terrimg;
	var terrstr;
	return function(arr) {
		var newstr = JSON.stringify(arr);
		if(newstr == terrstr)
			return terrimg;
		terrstr = newstr;
		terrimg = document.createElement("canvas");
		if((arr.length < 1) || (arr[0].length < 1))
			return terrimg;
		terrimg.height = (arr.length + 1) * BLK;
		terrimg.width = (arr[0].length + 1) * BLK;
		var ctx = terrimg.getContext("2d");
		for(var y = 0; y < arr.length; y++)
			for(var x = 0; x < arr[y].length; x++) {
				var r = Math.floor(Math.random() * 4) / 4;
				var i = arr[y][x] ? images.wall : images.floor;
				drawent(ctx, i, x, y, r);
			}
		return terrimg;
	};
})();

function render() {
	window.requestAnimationFrame(render);

	var now = new Date().getTime();
	frametimes.push(now);
	while(frametimes[0] < (now - 5000))
		frametimes.shift();
	
	var el = document.getElementById("main");
	var loading = document.getElementById("loading");

	var ready = isready() && (states.length > 0);
	setvis(loading, !ready);
	setvis(el, ready);
	if(!ready)
		return;

	statetick();
	var state = states[0];
	var sx = state.size.x;
	var sy = state.size.y;
	canvascale(el, (sx + 1) * BLK, (sy + 1) * BLK);

	var ctx = el.getContext("2d");

	var tanks = [];
	var terr = [];
	for(var y = 0; y < sy; y++) {
		var trow = [];
		terr.push(trow);
		for(var x = 0; x < sx; x++) {
			var ent = state.grid[y][x];
			trow.push(ent === true);
			if(ent && ent.t)
				tanks.push({x: x, y: y, ent: ent});
		}
	}

	ctx.drawImage(buildterrain(terr), 0, 0);
	for(var i = 0; i < tanks.length; i++)
		if(!tanks[i].ent.tw || !tanks[i].ent.tw.v)
			drawent(ctx, gettank(tanks[i].ent.o), tanks[i].x, tanks[i].y,
				tanks[i].ent.r, tanks[i].ent.tw);
	//for(var i = 0; i < tanks.length; i++)
		//shadetext(ctx, tanks[i].x, tanks[i].y + 1, 1,
			//tanks[i].ent.o, tanks[i].ent.tw);
	for(var i = 0; i < particles.length; i++)
		drawpart(ctx, particles[i]);
}
render();
