========================================================================
                             T A N K M A Z E
                            Example Player AI
------------------------------------------------------------------------

This is an example AI for the TankMaze game.  You can use this as a
basis for developing more advanced AI, including translation into any
language of your choice for which you can communicate with the play
agent.

------------------------------------------------------------------------
-- RULES OF THE GAME

Each player controls a tank on a 2D grid of squares.  Each square can
be either empty, occupied by a wall, or occupied by a tank.  The grid
itself is a flat torus, and wraps around to the other side from each of
the four boundaries.

The game is played in a series of turns, each of a configurable length
(commonly 500ms to 1000ms long).  At the beginning of the turn, tank
AI's are given a copy of the complete state of the game, including all
participants, scores, terrain, tank positions, etc.  Before the end of
the turn, each tank AI must submit a command for its tank in response.
AI that fail to respond in time will have tanks sit inactive.  The
possible commands are to turn (rotate) left, right, drive 1 grid square
forward, or fire the cannon.  Each action takes 1 turn, and all actions
are processed simultaneously.  If any actions conflict (i.e. more than
one tank trying to move into a square at the same time) then the
winner is chosen randomly, and conflicting actions are cancelled.

Each tank has a fixed forward-facing cannon with unlimited ammunition.
Cannon fire moves forward in a straight line in the direction the tank
is facing, for an unlimited range, until it hits a wall or a tank.
Cannon fire takes place at the BEGINNING of each turn, i.e. before
movement has had a chance to take place.

Each tank is equipped with a frontal shield, which protects it from (and
reflects) cannon fire so long as the tank is NOT firing its own cannon.

Hitting a tank in the rear or side face destroys it.  Hitting a tank in
the front shield, if it is NOT firing, causes the shot to reflect back
at the shooter (which destroys it, because its frontal shield is already
compromised by firing).  Hitting a tank in the front shield while it IS
firing will destroy the enemy tank (but the shooter will also still be
destroyed, since it will be hit by the enemy return fire).

Cannons require an empty space in front of the barrel to fire.  Trying
to fire a cannon point-blank at any obstacle (including a wall or other
tank) will cause the cannon to backfire and destroy the shooter (leaving
the target unharmed).

Each successfully destroyed enemy tank is worth one point each.  Each
suicide, i.e. by backfiring the cannon or being hit by your own fire, is
worth -1 point.

Score is also awarded for survival; 0.025 points are awarded for each
turn a tank is alive after the first 20 turns since it spawned.

If a tank is not on the field (i.e. after being destroyed, or before
first spawn), issuing any message to the server will spawn the tank.

------------------------------------------------------------------------
-- INCOMING MESSAGE FORMAT

The incoming message will be a copy of the game's complete current
internal state; the "game" distribution can be analyzed for specific
interpretation.

The overall structure is a document containing (at least):

- "turn": current turn number.
- "config": original configuration values for game, including:
	- "endturn": turn after which a tournament game freezes.
	- "restart": turn after which practice games restart.
- "size": the x/y dimensions of the grid.
- "grid": the objects on the playing field, addressed as grid[y][x].
- "players": list of players, each containing:
	- "n": player username.
	- "s": player's score.
- "fx": list of visual effects for viewers to render.

Each grid space contains:
	- a falsey value for unoccupied spaces.
	- boolean true for a wall.
	- an object with at least {t: true} for a tank.

Each tank contains:
	- "o": the index of the owning player in the "players" array
	- "r": the direction of the tank is facing, as
	       clockwise-of-north in revolutions, i.e. 0 for north,
	       0.25 for east, 0.5 for south, 0.75 for west.
	- "tw": tweening information, used by the viewer for animation.

------------------------------------------------------------------------
-- RESPONSE FORMAT

Each response message must be an object containing the following keys:

- "t": the turn number of the state to which the AI is responding.
- "x": the x location of the tank being commanded.
- "y": the y location of the tank being commanded.
- "c": the command to issue to the tank:
	- "r" to turn right.
	- "l" to turn left.
	- "f" to move forward.
	- "s" to shoot the cannon.

========================================================================
