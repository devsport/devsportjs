========================================================================
         DevSport: Competitive Programming as a Spectator Sport
------------------------------------------------------------------------

This repository contains the components of the DevSport nodejs
framework.  Look for a README file within each individal subdirectory.

------------------------------------------------------------------------
-- COMPONENTS

agent/
	The DevSport communications agents, which provide internet
	communication and interconnectivity.

	The agent contains 3 components:

	hub.js
		A websocket server that provides authentication and
		relays messages between hosts and players.

	host.js
		The host agent that wraps and manages a game host
		and provides connectivity between it and the hub.

	play.js
		The player agent that wraps a player's AI and
		provides connectivity between it and the hub.

game/
	Game server logic, to be connected to a hub via the host agent.

player/
	Player AI logic, to be connecetd to a hub via the play agent.

viewer/
	An HTML5/js viewer for watching live game streams in a browser.
	It should be deployed on a web server, and can connect to any
	game on any hub that has matching game logic.

testrig/
	A meta-agent that runs and interconnects all of the above
	components for local testing.

The "game", "player", and "viewer" components all implement specific
game logic, and are interdependent.  The "hub" and "testrig" components
are universal and can be used with multiple different potential games.

------------------------------------------------------------------------
-- SETUP

On unix-like systems, just run "make" to setup all dependencies.

On Windows, you may have to read the Makefile and run those steps
manually.  This includes running "npm install" in subdirectories that
contain package dependencies, and copying the msgpack browser
distribution script into the viewer subdir.

------------------------------------------------------------------------
-- JOINING A GAME

To join a game, read the documentation in the "agent" component,
particularly the documentation for the "play" agent.  It is assumed that
players will be reasonably familiar with development tools and
environments, or capable of doing some independent research to learn.

An example minimal AI is packaged in the "player" directory, as well as
some documentation about the rules of the specific game.  You can start
developing your AI from this base, including porting it to any platform
you want to use, and are able to connect to the play agent.

Note that some documentation may be sparse, either intentionally or
otherwise; there may be some components (particularly of the actual
game logic internals) that are meant to be part of the challenge.

If a practice game is available, you should use it to get familiar with
the tools, instead of waiting until a scheduled or timed game.

To develop your AI locally, without using a practice game, or exposing
your work-in-progress to public scrutiny, you can look into the
documentation for the "testrig" component.

========================================================================
